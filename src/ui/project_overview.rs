use crate::data::project::ProjectInfo;
use crate::ui::{DataView, DataViewResponse};
use eframe::egui::{self, FontId, RichText};

#[derive(Default)]
pub struct ProjectOverview {}

fn format_time(micros: u64) -> String {
    let ts = micros / 1_000_000;
    let tm = ts / 60;
    let h = tm / 60;
    let m = tm % 60;
    let s = ts % 60;

    if h > 0 {
        format!("{:2}:{:02}:{:02}", h, m, s)
    } else if m > 0 {
        format!("{:2}:{:02}", m, s)
    } else {
        format!("{:2}", s)
    }
}

impl DataView for ProjectOverview {
    fn render(&mut self, project_info: &ProjectInfo, ui: &mut egui::Ui) -> DataViewResponse {
        ui.vertical(|ui| {
            ui.heading("Project overview");
            ui.separator();
            ui.add_space(8.0);
            egui::ScrollArea::vertical()
                .auto_shrink([false; 2])
                .show_viewport(ui, |ui, _viewport| {
                    ui.label(format!(
                        "Translation units: {}",
                        project_info.translation_units.len()
                    ));

                    ui.label(format!("Headers: {}", project_info.sum_header_info.len()));
                    ui.separator();

                    let frontend = project_info.totals.get("Frontend").unwrap_or(&0);
                    let backend = project_info.totals.get("Backend").unwrap_or(&0);

                    ui.label(
                        RichText::new(format!("Total Time: {}", format_time(frontend + backend)))
                            .font(FontId::proportional(16.0)),
                    );
                    ui.label(format!("Total Frontend: {}", format_time(*frontend)));
                    ui.label(format!("Total Backend: {}", format_time(*backend)));
                    ui.separator();
                    project_info.totals.iter().for_each(|(k, v)| {
                        ui.label(
                            RichText::new(format!("Total {}: {}", k, format_time(*v)))
                                .font(FontId::proportional(10.0)),
                        );
                    });
                });
        });
        DataViewResponse::default()
    }

    fn initialise(&mut self, _project_info: &ProjectInfo) {}
}
