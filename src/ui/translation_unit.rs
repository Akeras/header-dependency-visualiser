use crate::data::project::{HashOrIdx, HeaderInfo, ProjectInfo};
use crate::ui::timeline_view::{
    LaneItem, TimeLineView, TimeLineViewDataError, TimeLineViewDataProvider, TimeLineViewResponse,
};
use crate::ui::{self, DataView, DataViewResponse};
use eframe::egui;
use rayon::slice::ParallelSliceMut;
use std::collections::HashMap;
use std::time::Duration;

use super::strip_json_name;

pub struct TranslationUnitView {
    lane_data: Vec<Vec<LaneItem>>,
    header_infos: HashMap<HashOrIdx, HeaderInfo>,
    active_tu: u64,
    selected_header: Option<HashOrIdx>,
    time_line_view: TimeLineView,
}

struct TranslationUnitViewDataProvider<'a> {
    lane_data: &'a Vec<Vec<LaneItem>>,
    project_info: &'a ProjectInfo,
    header_infos: &'a HashMap<HashOrIdx, HeaderInfo>,
    active_tu: u64,
    selected_header: &'a Option<HashOrIdx>,
}

impl TranslationUnitView {
    pub fn new(tu: u64) -> Self {
        TranslationUnitView {
            lane_data: Vec::<Vec<LaneItem>>::new(),
            header_infos: HashMap::<HashOrIdx, HeaderInfo>::new(),
            active_tu: tu,
            selected_header: None,
            time_line_view: TimeLineView::default(),
        }
    }
}

impl<'a> TranslationUnitViewDataProvider<'a> {
    fn new(
        lane_data: &'a Vec<Vec<LaneItem>>,
        project_info: &'a ProjectInfo,
        header_infos: &'a HashMap<HashOrIdx, HeaderInfo>,
        active_tu: u64,
        selected_header: &'a Option<HashOrIdx>,
    ) -> Self {
        Self {
            lane_data,
            project_info,
            header_infos,
            active_tu,
            selected_header,
        }
    }

    fn get_selected_header_label(&self) -> String {
        match self.selected_header.as_ref() {
            Some(header) => match header {
                HashOrIdx::Hash(h) => {
                    let sel_h_name = self.project_info.files_hash.get(h);
                    let sel_h_info = self.header_infos.get(header);
                    let sel_h_sum = self.project_info.sum_header_info.get(h);

                    let label1 = match sel_h_name {
                        Some(hn) => ui::strip_path(hn).to_string(),
                        None => "".to_string(),
                    };
                    let label2 = match sel_h_info {
                        Some(hi) => format!(" TU time: {:.2?},", Duration::from_micros(hi.dur)),
                        None => "".to_string(),
                    };
                    let label3 = match sel_h_sum {
                        Some(hs) => format!(
                            " Avg: {:.2?}, Inc: {}, Total: {:.2?}",
                            Duration::from_micros(hs.dur / hs.inc),
                            hs.inc,
                            Duration::from_micros(hs.dur)
                        ),
                        None => "".to_string(),
                    };

                    label1 + &label2 + &label3
                }
                HashOrIdx::Idx(i) => {
                    let label = self.project_info.non_files[*i as usize].to_string();
                    let label2 = match self.header_infos.get(header) {
                        Some(hi) => format!(" TU time: {:.2?},", Duration::from_micros(hi.dur)),
                        None => "".to_string(),
                    };

                    label + &label2
                }
            },
            None => " ".to_string(),
        }
    }
}
impl<'a> TimeLineViewDataProvider<'a> for TranslationUnitViewDataProvider<'a> {
    fn get_title(&self) -> Result<String, TimeLineViewDataError> {
        match self.project_info.files_hash.get(&self.active_tu) {
            Some(filename) => Ok("Translation unit: ".to_string() + strip_json_name(filename)),
            None => Err(TimeLineViewDataError::DataError),
        }
    }

    fn get_top_info(&self) -> Result<String, TimeLineViewDataError> {
        Ok(self.get_selected_header_label())
    }

    fn get_start(&self) -> u64 {
        0
    }

    fn get_end(&self) -> u64 {
        let dur = match self.project_info.translation_units.get(&self.active_tu) {
            Some(tu) => tu.dur,
            None => 0,
        };
        dur
    }

    fn get_lane_numbers(&self) -> usize {
        self.lane_data.len()
    }

    fn get_lane_len(&self, idx: usize) -> Result<usize, TimeLineViewDataError> {
        if idx >= self.lane_data.len() {
            return Err(TimeLineViewDataError::DataError);
        }
        Ok(self.lane_data[idx].len())
    }

    fn get_lane_item(
        &self,
        lane_idx: usize,
        item_idx: usize,
    ) -> Result<&'a LaneItem, TimeLineViewDataError> {
        if lane_idx >= self.lane_data.len() || item_idx >= self.lane_data[lane_idx].len() {
            return Err(TimeLineViewDataError::DataError);
        }
        Ok(&self.lane_data[lane_idx][item_idx])
    }

    fn get_lane_item_name(&self, hash_or_idx: &HashOrIdx) -> Result<&str, TimeLineViewDataError> {
        let name = match hash_or_idx {
            HashOrIdx::Hash(h) => Ok(ui::strip_path(match self.project_info.files_hash.get(h) {
                Some(str) => str,
                None => return Err(TimeLineViewDataError::DataError),
            })),
            HashOrIdx::Idx(i) => {
                if *i as usize >= self.project_info.non_files.len() {
                    return Err(TimeLineViewDataError::DataError);
                }
                return Ok(&self.project_info.non_files[*i as usize]);
                // + &format!("{:.2?}", Duration::from_micros(hi.dur))
            }
        };
        name
    }
}

impl DataView for TranslationUnitView {
    fn render(&mut self, project_info: &ProjectInfo, ui: &mut egui::Ui) -> DataViewResponse {
        let data_provider = TranslationUnitViewDataProvider::new(
            &self.lane_data,
            project_info,
            &self.header_infos,
            self.active_tu,
            &self.selected_header,
        );

        let mut response = DataViewResponse::default();
        if let Ok(r) = self.time_line_view.render(&data_provider, ui) {
            match r {
                TimeLineViewResponse::DblClicked(h_or_i) => match h_or_i {
                    HashOrIdx::Hash(h) => response.activated_header = Some(h),
                    HashOrIdx::Idx(_) => {}
                },
                TimeLineViewResponse::Clicked(h_or_i) => self.selected_header = Some(h_or_i),
                TimeLineViewResponse::None => {}
            }
        }

        response
    }

    fn initialise(&mut self, project_info: &ProjectInfo) {
        let mut hi_vec: Vec<&HeaderInfo> = project_info
            .header_infos
            .iter()
            .filter(|hi| hi.translation_unit == self.active_tu)
            .collect();

        hi_vec.par_sort_unstable_by(|&a, &b| {
            if a.start == b.start {
                (a.start + a.dur).cmp(&(b.start + b.dur))
            } else {
                a.start.cmp(&b.start)
            }
        });
        hi_vec.iter().for_each(|hi| {
            self.header_infos.insert(hi.hash_or_idx, (*hi).clone());
        });

        let mut lanes = Vec::<Vec<LaneItem>>::new();
        for hi in &hi_vec {
            let mut lane_index: usize = 0;
            if !lanes.is_empty() {
                let mut dist = 10e9 as u64;
                let mut l_idx = 0;
                let mut found = false;
                for lane in &lanes {
                    let lane_last = lane.last().unwrap();
                    let lane_end = lane_last.start + lane_last.dur;
                    if hi.start >= lane_end {
                        let d = hi.start - lane_end;
                        if d < dist {
                            dist = d;
                            l_idx = lane_index;
                            found = true;
                        }
                    }
                    lane_index += 1;
                }
                if found {
                    lane_index = l_idx;
                }
            }
            if lane_index == lanes.len() {
                lanes.push(Vec::<LaneItem>::new());
            }
            let lane_item = LaneItem {
                hash_or_idx: hi.hash_or_idx,
                start: hi.start,
                dur: hi.dur,
            };
            lanes[lane_index].push(lane_item);
        }
        self.lane_data = lanes;
    }
}
