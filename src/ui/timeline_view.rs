use crate::data::project::HashOrIdx;
use crate::ui::{self, hdv_app::HDVApp};
use eframe::egui;
use eframe::emath::{Align2, NumExt};

pub struct LaneItem {
    pub hash_or_idx: HashOrIdx,
    pub start: u64,
    pub dur: u64,
}

pub enum TimeLineViewDataError {
    DataError,
}

pub trait TimeLineViewDataProvider<'a> {
    fn get_title(&self) -> Result<String, TimeLineViewDataError>;
    fn get_top_info(&self) -> Result<String, TimeLineViewDataError>;

    fn get_start(&self) -> u64;
    fn get_end(&self) -> u64;

    fn get_lane_numbers(&self) -> usize;
    fn get_lane_len(&self, idx: usize) -> Result<usize, TimeLineViewDataError>;
    fn get_lane_item(
        &self,
        lane_idx: usize,
        item_idx: usize,
    ) -> Result<&'a LaneItem, TimeLineViewDataError>;

    fn get_lane_item_name(&self, hash_or_idx: &HashOrIdx) -> Result<&str, TimeLineViewDataError>;
}

pub enum TimeLineViewResponse {
    Clicked(HashOrIdx),
    DblClicked(HashOrIdx),
    None,
}

enum TimeLineViewInteraction {
    DblClick,
    Click,
    Hover,
    None,
}

pub struct TimeLineView {
    zf: f32,
    prev_zf: f32,
    last_viewport: Option<egui::Rect>,
    active_item: Option<HashOrIdx>,
}

impl Default for TimeLineView {
    fn default() -> Self {
        TimeLineView {
            zf: 1.0,
            prev_zf: 1.0,
            last_viewport: None,
            active_item: None,
        }
    }
}

impl TimeLineView {
    fn render_header<'x, T>(
        &mut self,
        data_provider: &'x T,
        ui: &mut egui::Ui,
    ) -> Result<(), TimeLineViewDataError>
    where
        T: TimeLineViewDataProvider<'x>,
    {
        self.prev_zf = self.zf;
        ui.vertical(|ui| -> Result<(), TimeLineViewDataError> {
            ui.heading(&data_provider.get_title()?);
            ui.horizontal(|ui| {
                ui.label("Zoom: ".to_string());
                ui.add(egui::Slider::new(&mut self.zf, 0.1..=10.0).logarithmic(true));
            });

            ui.label(data_provider.get_top_info()?);
            Ok(())
        });
        Ok(())
    }

    fn time_span<'x, T>(data_provider: &'x T) -> f32
    where
        T: TimeLineViewDataProvider<'x>,
    {
        let time_span = ((data_provider.get_end() - data_provider.get_start()) / 1000) as f32;
        1000.0 * f32::ceil(time_span / 1000.0)
    }

    fn calculate_lane_item_rect<'x, T>(
        data_provider: &'x T,
        viewport: &egui::Rect,
        lane_cnt: usize,
        lane_idx: usize,
        lane_item: &LaneItem,
        width: f32,
    ) -> egui::Rect
    where
        T: TimeLineViewDataProvider<'x>,
    {
        let vertical_unit = viewport.height() / (lane_cnt + 1) as f32;
        let ey = viewport.bottom() - lane_idx as f32 * vertical_unit - 2.0;
        let sy = viewport.bottom() - (lane_idx + 1) as f32 * vertical_unit + 2.0;
        let st = ((lane_item.start - data_provider.get_start()) / 1000) as f32;
        let en = ((lane_item.start + lane_item.dur - data_provider.get_start()) / 1000) as f32;
        let sx = width * st / Self::time_span(data_provider);
        let ex = width * en / Self::time_span(data_provider);
        egui::Rect::from_min_max(egui::pos2(sx, sy), egui::pos2(ex, ey))
    }

    fn get_interaction(ui: &egui::Ui) -> (TimeLineViewInteraction, Option<egui::Pos2>) {
        ui.input(|i| -> (TimeLineViewInteraction, Option<egui::Pos2>) {
            if i.pointer
                .button_double_clicked(egui::PointerButton::Primary)
            {
                return (TimeLineViewInteraction::DblClick, i.pointer.interact_pos());
            }
            if i.pointer.primary_clicked() {
                return (TimeLineViewInteraction::Click, i.pointer.interact_pos());
            }
            if i.pointer.has_pointer() {
                return (TimeLineViewInteraction::Hover, i.pointer.hover_pos());
            }
            (TimeLineViewInteraction::None, None)
        })
    }

    fn render_lane_item<'x, T>(
        &self,
        data_provider: &'x T,
        lane_item: &LaneItem,
        lane_item_rect: &egui::Rect,
        ui: &egui::Ui,
        hovered: bool,
    ) -> Result<(), TimeLineViewDataError>
    where
        T: TimeLineViewDataProvider<'x>,
    {
        let active = match self.active_item {
            Some(t) => lane_item.hash_or_idx == t,
            None => false,
        };
        let extra_color = if hovered {
            64
        } else if active {
            32
        } else {
            0
        };
        ui.painter().rect(
            *lane_item_rect,
            2.0,
            egui::Color32::from_rgb(
                ((lane_item.hash_or_idx.value() % 16) as u8) * 4 + 16 + extra_color,
                extra_color,
                extra_color,
            ),
            if active {
                egui::Stroke::new(1.0, egui::Color32::WHITE)
            } else {
                egui::Stroke::new(0.0, egui::Color32::TRANSPARENT)
            },
        );

        let name = data_provider.get_lane_item_name(&lane_item.hash_or_idx)?;

        let my_font_id = egui::FontId::new(10.0, egui::FontFamily::Monospace);
        let my_color = egui::Color32::from_rgb(192, 192, 192);
        ui.painter().text(
            egui::pos2(lane_item_rect.left() + 5.0, lane_item_rect.center().y),
            Align2::LEFT_CENTER,
            ui::ellide_text(name, lane_item_rect.width() - 10.0, ui, &my_font_id),
            my_font_id,
            my_color,
        );
        Ok(())
    }

    fn render_scroll_area<'x, T>(
        &mut self,
        data_provider: &'x T,
        ui: &mut egui::Ui,
    ) -> Result<TimeLineViewResponse, TimeLineViewDataError>
    where
        T: TimeLineViewDataProvider<'x>,
    {
        let mut response = TimeLineViewResponse::None;

        let width = self.zf * 5_000.0_f32;
        let mut scroll_area = egui::ScrollArea::horizontal();
        if self.prev_zf != self.zf && self.last_viewport.is_some() {
            let ratio = self.zf / self.prev_zf;
            let vp = self.last_viewport.unwrap();
            let x_offset = (vp.center().x * ratio - 0.5 * vp.width())
                .clamp(0.0, (width - vp.width()).at_least(0.0));
            scroll_area = scroll_area.horizontal_scroll_offset(x_offset);
        }
        scroll_area.auto_shrink([false; 2]).show_viewport(
            ui,
            |ui, viewport| -> Result<(), TimeLineViewDataError> {
                self.last_viewport = Some(viewport);
                ui.set_width(width);
                let interaction = Self::get_interaction(ui);
                let lane_cnt = data_provider.get_lane_numbers();
                for lane_idx in 0..lane_cnt {
                    for item_idx in 0..data_provider.get_lane_len(lane_idx)? {
                        let lane_item = data_provider.get_lane_item(lane_idx, item_idx)?;
                        let li_rect = Self::calculate_lane_item_rect(
                            data_provider,
                            &viewport,
                            lane_cnt,
                            lane_idx,
                            lane_item,
                            width,
                        );

                        if !viewport.intersects(li_rect) {
                            continue;
                        }
                        let li_rect = li_rect.translate(ui.min_rect().left_top().to_vec2());

                        let mut hovered = false;
                        if interaction.1.is_some() && li_rect.contains(interaction.1.unwrap()) {
                            match interaction.0 {
                                TimeLineViewInteraction::DblClick => {
                                    response =
                                        TimeLineViewResponse::DblClicked(lane_item.hash_or_idx);
                                }
                                TimeLineViewInteraction::Click => {
                                    self.active_item = Some(lane_item.hash_or_idx);
                                    response = TimeLineViewResponse::Clicked(lane_item.hash_or_idx);
                                }
                                TimeLineViewInteraction::Hover => {
                                    hovered = true;
                                }
                                TimeLineViewInteraction::None => (),
                            }
                        }

                        self.render_lane_item(data_provider, lane_item, &li_rect, ui, hovered)?;
                    }
                }
                HDVApp::render_grid(
                    &viewport,
                    self.zf,
                    width,
                    Self::time_span(data_provider),
                    ui,
                );
                Ok(())
            },
        );
        Ok(response)
    }

    pub fn render<'x, T>(
        &mut self,
        data_provider: &'x T,
        ui: &mut egui::Ui,
    ) -> Result<TimeLineViewResponse, TimeLineViewDataError>
    where
        T: TimeLineViewDataProvider<'x>,
    {
        self.render_header(data_provider, ui)?;
        self.render_scroll_area(data_provider, ui)
    }
}
