use crate::data::project::{HashOrIdx, ProjectInfo};
use crate::ui::{self, DataView, DataViewResponse};
use eframe::egui;
use eframe::emath::Align2;
use rayon::slice::ParallelSliceMut;
use std::time::Duration;

#[derive(Default, Clone)]
pub struct HeaderInTU {
    pub tu_hash: u64,
    pub dur: u64,
}

impl HeaderInTU {
    pub fn new(tu_hash: u64, dur: u64) -> Self {
        HeaderInTU { tu_hash, dur }
    }
}

pub struct HeaderView {
    data: Vec<HeaderInTU>,
    header: u64,
    header_name: String,
    active_tu: Option<u64>,
}

impl HeaderView {
    pub fn new(header: u64) -> Self {
        HeaderView {
            data: Vec::<HeaderInTU>::new(),
            header,
            header_name: String::new(),
            active_tu: None,
        }
    }
}

impl DataView for HeaderView {
    fn render(&mut self, project_info: &ProjectInfo, ui: &mut egui::Ui) -> DataViewResponse {
        ui.vertical(|ui| {
            ui.heading("Heaviest inclusions of ".to_owned() + ui::strip_path(&self.header_name));
            if self.active_tu.is_some() {
                let empty_string = "".to_string();
                let active_tu = project_info
                    .files_hash
                    .get(&self.active_tu.unwrap())
                    .unwrap_or(&empty_string);
                let active_tu_dur = self
                    .data
                    .iter()
                    .find(|&h_in_tu| h_in_tu.tu_hash == self.active_tu.unwrap())
                    .unwrap()
                    .dur;
                ui.label(format!(
                    "Duration in {}: {:.2?}s)",
                    ui::strip_json_name(active_tu),
                    Duration::from_micros(active_tu_dur),
                ));
            } else {
                ui.label(" ");
            }
        });

        let mut response = DataViewResponse::default();
        egui::ScrollArea::vertical()
            .auto_shrink([false; 2])
            .show_viewport(ui, |ui, viewport| {
                ui.set_height(2_000.0);
                let x = ui.min_rect().left();
                let y = ui.min_rect().top();

                let mut hover_pos: Option<egui::Pos2> = None;
                let mut clicked_pos: Option<egui::Pos2> = None;
                let mut dbl_clicked_pos: Option<egui::Pos2> = None;
                ui.input(|i| {
                    if i.pointer.has_pointer() {
                        hover_pos = i.pointer.hover_pos();
                    }
                    if i.pointer.primary_clicked() {
                        clicked_pos = i.pointer.interact_pos();
                    }
                    if i.pointer
                        .button_double_clicked(egui::PointerButton::Primary)
                    {
                        dbl_clicked_pos = i.pointer.interact_pos();
                    }
                });
                let vp_width = viewport.width();
                let d_width = (self.data[0].dur / 1000) as f32;

                let x_padding = 2.0;

                let mut y_idx = 0.0;
                for header_in_tu in &self.data {
                    let sy = y + y_idx * 20.0;
                    let ey = sy + 18.0;

                    let sx = x + x_padding;
                    let ex = x + vp_width * (header_in_tu.dur / 1000) as f32 / d_width - x_padding;

                    let tu_rect = egui::Rect::from_min_max(egui::pos2(sx, sy), egui::pos2(ex, ey));
                    let hovered = match hover_pos {
                        Some(pos) => tu_rect.contains(pos),
                        None => false,
                    };
                    let clicked = match clicked_pos {
                        Some(pos) => tu_rect.contains(pos),
                        None => false,
                    };
                    if clicked {
                        self.active_tu = Some(header_in_tu.tu_hash);
                    }
                    let dbl_clicked = match dbl_clicked_pos {
                        Some(pos) => tu_rect.contains(pos),
                        None => false,
                    };
                    if dbl_clicked {
                        response.activated_tu = Some(header_in_tu.tu_hash);
                    }
                    let active = match self.active_tu {
                        Some(h) => header_in_tu.tu_hash == h,
                        None => false,
                    };
                    let extra_color = if hovered {
                        64
                    } else if active {
                        32
                    } else {
                        0
                    };

                    ui.painter().rect(
                        tu_rect,
                        2.0,
                        egui::Color32::from_rgb(
                            ((header_in_tu.tu_hash % 16) as u8) * 4 + 16 + extra_color,
                            extra_color,
                            extra_color,
                        ),
                        if active {
                            egui::Stroke::new(1.0, egui::Color32::WHITE)
                        } else {
                            egui::Stroke::new(0.0, egui::Color32::TRANSPARENT)
                        },
                    );

                    let name = ui::strip_json_name(
                        project_info.files_hash.get(&header_in_tu.tu_hash).unwrap(),
                    );

                    let my_font_id = egui::FontId::new(10.0, egui::FontFamily::Monospace);
                    let my_color = egui::Color32::from_rgb(192, 192, 192);
                    ui.painter().text(
                        egui::pos2(sx + 5.0, (sy + ey) / 2.0),
                        Align2::LEFT_CENTER,
                        ui::ellide_text(name, ex - sx - 10.0, ui, &my_font_id),
                        my_font_id,
                        my_color,
                    );
                    y_idx += 1.0;
                }
            });
        response
    }

    fn initialise(&mut self, project_info: &ProjectInfo) {
        project_info.header_infos.iter().for_each(|hi| {
            match hi.hash_or_idx {
                HashOrIdx::Hash(h) => {
                    if h == self.header {
                        self.data.push(HeaderInTU::new(hi.translation_unit, hi.dur));
                    }
                }
                HashOrIdx::Idx(_i) => (),
            };
        });

        // let newsize = 10.at_most(self.data.len());
        self.data.par_sort_unstable_by(|a, b| b.dur.cmp(&a.dur));

        // self.data.resize(newsize, HeaderInTU::default());
        self.header_name = project_info
            .files_hash
            .get(&self.header)
            .unwrap_or(&"".to_owned())
            .to_owned();
    }
}
