pub mod hdv_app;
mod header;
mod project_overview;
mod project_timeline;
mod top_headers;
mod top_translation_units;
mod translation_unit;
mod timeline_view;

use crate::data::project::ProjectInfo;
use eframe::egui;

fn strip_json_name(s: &str) -> &str {
    let pos = s.rfind('/');
    match pos {
        Some(p) => match s.rfind(".json") {
            Some(q) => &s[p + 1..q],
            None => &s[p + 1..s.len()],
        },
        None => s,
    }
}

fn strip_path(s: &str) -> &str {
    let pos = s.rfind('/');
    match pos {
        Some(p) => &s[p + 1..s.len()],
        None => s,
    }
}

fn ellide_text(s: &str, available_width: f32, ui: &egui::Ui, font_id: &egui::FontId) -> String {
    let str_galley =
        ui.painter()
            .layout(s.to_owned(), font_id.clone(), egui::Color32::BLACK, 1200.0);

    if str_galley.as_ref().rect.width() < available_width {
        return s.to_owned();
    } else {
        let c_idx = str_galley
            .cursor_from_pos(egui::vec2(available_width, 0.0))
            .ccursor
            .index;
        if c_idx == s.len() {
            return s.to_owned();
        }
        if c_idx < s.len() {
            if c_idx > 1 {
                return s[0..c_idx - 1].to_string() + "…";
            } else {
                return s[0..c_idx].to_string();
            }
        }
    }

    "".to_string()
}

fn zoom_dependent_grid_step(zf: f32) -> f32 {
    if zf < 0.5 {
        60_000.0
    } else if zf < 1.0 {
        30_000.0
    } else if zf < 2.0 {
        10_000.0
    } else if zf < 4.0 {
        5_000.0
    } else if zf < 6.0 {
        2_000.0
    } else {
        1_000.0
    }
}

#[derive(Default)]
struct DataViewResponse {
    activated_tu: Option<u64>,
    activated_header: Option<u64>,
}
trait DataView {
    fn render(&mut self, project_info: &ProjectInfo, ui: &mut egui::Ui) -> DataViewResponse;
    fn initialise(&mut self, project_info: &ProjectInfo);
}
