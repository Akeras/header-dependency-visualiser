use crate::data;
use crate::data::project::{ProjectInfo, TranslationUnit};
use crate::ui::header::HeaderView;
use crate::ui::project_overview::ProjectOverview;
use crate::ui::project_timeline::ProjectTimeline;
use crate::ui::top_headers::TopHeaders;
use crate::ui::top_translation_units::TopTranslationUnits;
use crate::ui::translation_unit::TranslationUnitView;
use crate::ui::zoom_dependent_grid_step;
use crate::ui::DataView;
use eframe::egui;
use eframe::emath::Align2;
use std::boxed::Box;
use std::sync::mpsc::{self, Receiver, Sender};
use std::thread::{self, JoinHandle};

use std::time::Duration;
#[derive(PartialEq)]
enum ViewType {
    ProjectOverview,
    ProjectTimeline,
    TopTranslationUnits,
    TopHeaders,
    TranslationUnitView(u64),
    HeaderView(u64),
}

pub struct HDVApp {
    show_config: bool,
    build_dir: String,
    handle: Option<JoinHandle<Option<ProjectInfo>>>,
    project_info: Option<ProjectInfo>,
    channel: (Sender<String>, Receiver<String>),
    log: String,
    lane_data: Vec<Vec<TranslationUnit>>,
    view_type: ViewType,
    current_view: Option<Box<dyn DataView>>,
}

impl Default for HDVApp {
    fn default() -> Self {
        Self {
            show_config: true,
            build_dir: "".to_string(),
            handle: None,
            project_info: None,
            channel: mpsc::channel::<String>(),
            log: String::new(),
            lane_data: Vec::<Vec<TranslationUnit>>::new(),
            view_type: ViewType::ProjectTimeline,
            current_view: None,
        }
    }
}

impl HDVApp {
    pub fn new(cc: &eframe::CreationContext<'_>) -> Self {
        let mut style: egui::Style = (*cc.egui_ctx.style()).clone();

        style.spacing.item_spacing = egui::Vec2::new(7.0, 7.0);
        style.spacing.button_padding = egui::Vec2::new(18.0, 6.0);
        style.visuals.override_text_color = Some(egui::Color32::from_rgb(235, 235, 235));

        style.visuals.widgets.noninteractive.bg_stroke.color =
            egui::Color32::from_rgb(205, 205, 205);
        style.visuals.widgets.noninteractive.bg_fill = egui::Color32::from_rgb(25, 25, 25);
        style.visuals.widgets.noninteractive.weak_bg_fill = egui::Color32::from_rgb(48, 0, 0);
        style.visuals.widgets.inactive.bg_fill = egui::Color32::from_rgb(48, 0, 0);
        style.visuals.widgets.inactive.weak_bg_fill = egui::Color32::from_rgb(48, 0, 0);
        style.visuals.widgets.hovered.bg_fill = egui::Color32::from_rgb(80, 0, 0);
        style.visuals.widgets.hovered.weak_bg_fill = egui::Color32::from_rgb(80, 0, 0);
        style.visuals.widgets.active.weak_bg_fill = egui::Color32::from_rgb(96, 32, 32);
        style.visuals.widgets.active.bg_fill = egui::Color32::from_rgb(96, 32, 32);
        style.visuals.widgets.active.bg_fill = egui::Color32::from_rgb(32, 0, 0);
        style.visuals.widgets.active.fg_stroke.color = egui::Color32::from_rgb(235, 235, 235);

        style.visuals.window_fill = egui::Color32::from_rgb(0, 0, 0);

        style.visuals.extreme_bg_color = egui::Color32::BLACK;

        style.visuals.selection.bg_fill = egui::Color32::from_rgb(192, 128, 128);
        style.visuals.selection.stroke.color = egui::Color32::from_rgb(192, 128, 128);
        style.spacing.slider_width = 200.0;

        cc.egui_ctx.set_style(style);

        Default::default()
    }

    fn render_config(&mut self, ctx: &egui::Context) {
        let busy = self.handle.is_some();
        let left_frame = egui::Frame::none()
            .inner_margin(egui::Margin::same(11.0))
            .fill(egui::Color32::from_rgb(30, 10, 10));

        egui::SidePanel::left("left_panel")
            .resizable(false)
            .default_width(250.0)
            .frame(left_frame)
            .show_animated(ctx, self.show_config, |ui| {
                ui.vertical(|ui| {
                    ui.set_enabled(!busy);
                    ui.heading("Configuration");
                    ui.separator();
                    ui.add_space(4.0);
                    ui.label("Build directory:");
                    ui.text_edit_singleline(&mut self.build_dir);
                    ui.add_space(8.0);
                    ui.vertical_centered(|ui| {
                        if ui.button("Configure").clicked() {
                            self.current_view = None;
                            get_data(self);
                        }
                    });
                });
                ui.vertical(|ui| {
                    ui.set_enabled(!busy && self.project_info.is_some());
                    ui.add_space(12.0);
                    ui.heading("Views");
                    ui.separator();
                    ui.add_space(4.0);
                    ui.vertical_centered(|ui| {
                        if ui.button("Project Overview").clicked() {
                            self.init_view(ViewType::ProjectOverview);
                        }
                        if ui.button("Project Timeline").clicked() {
                            self.init_view(ViewType::ProjectTimeline);
                        }
                        if ui.button("Top TUs").clicked() {
                            self.init_view(ViewType::TopTranslationUnits);
                        }
                        if ui.button("Top headers").clicked() {
                            self.init_view(ViewType::TopHeaders);
                        }
                    });
                });
            });
    }

    fn render_log(&mut self, ctx: &egui::Context) {
        let bottom_frame = egui::Frame::none()
            .inner_margin(egui::Margin::same(11.0))
            .fill(egui::Color32::from_rgb(10, 10, 10));

        egui::TopBottomPanel::bottom("log_panel")
            .max_height(100.0)
            .default_height(100.0)
            .frame(bottom_frame)
            .show(ctx, |ui| {
                egui::ScrollArea::vertical()
                    .auto_shrink([false; 2])
                    .show_viewport(ui, |ui, _viewport| {
                        ui.add_sized(
                            ui.available_size(),
                            egui::TextEdit::multiline(&mut self.log),
                        )
                    });
            });
    }

    pub fn render_grid(viewport: &egui::Rect, zf: f32, width: f32, span: f32, ui: &egui::Ui) {
        let grid_step = zoom_dependent_grid_step(zf);
        let st_time = f32::floor(viewport.left() / width * span / grid_step) * grid_step;
        let en_time = f32::ceil(viewport.right() / width * span / grid_step) * grid_step;
        let mut time = st_time - grid_step;
        let x = ui.min_rect().left();
        let y = ui.min_rect().top();
        while time < en_time {
            time += grid_step;
            let x_time = x + time / span * width;
            ui.painter().line_segment(
                [
                    egui::pos2(x_time, y + viewport.top()),
                    egui::pos2(x_time, y + viewport.bottom()),
                ],
                egui::Stroke::new(1.0, egui::Color32::from_rgba_premultiplied(64, 64, 64, 64)),
            );
            let my_font_id = egui::FontId::new(10.0, egui::FontFamily::Monospace);
            let my_color = egui::Color32::from_rgb(192, 192, 192);
            ui.painter().text(
                egui::pos2(x_time + 5.0, y + viewport.top() + 5.0),
                Align2::LEFT_TOP,
                format!("{:?}", Duration::from_millis(time as u64)),
                my_font_id,
                my_color,
            );
        }
    }

    fn render_central_panel(&mut self, ctx: &egui::Context) {
        let busy = self.handle.is_some();
        let cp_frame = egui::Frame::none()
            .inner_margin(egui::Margin::same(11.0))
            .fill(egui::Color32::from_rgb(25, 35, 35));
        egui::CentralPanel::default()
            .frame(cp_frame)
            .show(ctx, |ui| {
                if !busy {
                    if self.project_info.is_some() {
                        // we have a project_info, but no view, create the default
                        if self.current_view.is_none() {
                            self.init_view(ViewType::ProjectOverview);
                        }
                        match &mut self.current_view {
                            Some(view) => {
                                let response = view.render(self.project_info.as_ref().unwrap(), ui);
                                if let Some(tu) = response.activated_tu {
                                    self.init_view(ViewType::TranslationUnitView(tu));
                                } else if let Some(header) = response.activated_header {
                                    self.init_view(ViewType::HeaderView(header));
                                }
                            }
                            None => (),
                        };
                    }
                } else {
                    ui.vertical_centered_justified(|ui| {
                        ui.spinner();
                        ui.label("Loading, please wait...");
                    });
                }
            });
    }

    fn init_view(&mut self, vt: ViewType) {
        if self.project_info.is_none() {
            return;
        }
        if self.current_view.is_some() && self.view_type == vt {
            return;
        }
        let mut view: Box<dyn DataView> = match vt {
            ViewType::ProjectOverview => Box::<ProjectOverview>::default(),
            ViewType::ProjectTimeline => Box::<ProjectTimeline>::default(),
            ViewType::TopTranslationUnits => Box::<TopTranslationUnits>::default(),
            ViewType::TopHeaders => Box::<TopHeaders>::default(),
            ViewType::TranslationUnitView(tu) => Box::new(TranslationUnitView::new(tu)),
            ViewType::HeaderView(header) => Box::new(HeaderView::new(header)),
        };
        view.initialise(self.project_info.as_ref().unwrap());
        self.current_view = Some(view);
        self.view_type = vt;
    }

    fn check_process(&mut self) -> bool {
        if self.handle.is_none() {
            return false;
        }

        if !self.handle.as_ref().unwrap().is_finished() {
            return true;
        }

        let join_res = self.handle.take().unwrap().join();
        self.project_info = match join_res {
            Ok(res) => res,
            Err(_) => None,
        };

        false
    }

    fn check_log(&mut self) -> bool {
        let s = match self.channel.1.try_recv() {
            Ok(s) => s,
            Err(_) => return false,
        };
        self.log += &s;
        self.log += "\n";
        true
    }
}

impl eframe::App for HDVApp {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        if self.check_process() || self.check_log() {
            ctx.request_repaint_after(Duration::from_millis(16));
        }
        self.render_config(ctx);
        self.render_log(ctx);
        self.render_central_panel(ctx);
    }
}

fn get_data(app: &mut HDVApp) {
    app.log.clear();
    app.lane_data.clear();
    let tx1 = app.channel.0.clone();
    let build_dir = app.build_dir.clone();
    app.handle = Some(thread::spawn(move || {
        data::process_project(&build_dir, tx1)
    }));
}
