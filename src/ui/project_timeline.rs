use crate::data::project::{HashOrIdx, ProjectInfo, TranslationUnit};
use crate::ui::timeline_view::{
    LaneItem, TimeLineView, TimeLineViewDataError, TimeLineViewDataProvider,
};
use crate::ui::{self, DataView, DataViewResponse};
use eframe::egui;
use rayon::slice::ParallelSliceMut;
use std::time::Duration;

use super::timeline_view::TimeLineViewResponse;

#[derive(Default)]
pub struct ProjectTimeline {
    lane_data: Vec<Vec<LaneItem>>,
    active_tu: Option<u64>,
    time_line_view: TimeLineView,
}

struct ProjectTimeLineDataProvider<'a> {
    project_info: &'a ProjectInfo,
    lane_data: &'a Vec<Vec<LaneItem>>,
    active_tu: &'a Option<u64>,
}

impl<'a> ProjectTimeLineDataProvider<'a> {
    fn new(
        project_info: &'a ProjectInfo,
        lane_data: &'a Vec<Vec<LaneItem>>,
        active_tu: &'a Option<u64>,
    ) -> Self {
        Self {
            project_info,
            lane_data,
            active_tu,
        }
    }
}

impl<'a> TimeLineViewDataProvider<'a> for ProjectTimeLineDataProvider<'a> {
    fn get_title(&self) -> Result<String, TimeLineViewDataError> {
        Ok("Project timeline".to_owned())
    }

    fn get_top_info(&self) -> Result<String, TimeLineViewDataError> {
        if self.active_tu.is_some() {
            let active_tu_name = match self.project_info.files_hash.get(&self.active_tu.unwrap()) {
                Some(tu) => tu,
                None => return Err(TimeLineViewDataError::DataError),
            };
            let tu = match self
                .project_info
                .translation_units
                .get(&self.active_tu.unwrap())
            {
                Some(tu) => tu,
                None => return Err(TimeLineViewDataError::DataError),
            };

            let avg = tu.dur;
            let st = tu.start - self.project_info.global_start;

            return Ok(format!(
                "{}: ({:.2?} - {:.2?})",
                ui::strip_json_name(active_tu_name),
                Duration::from_micros(st),
                Duration::from_micros(st + avg)
            ));
        }
        Ok("".to_owned())
    }

    fn get_start(&self) -> u64 {
        self.project_info.global_start
    }

    fn get_end(&self) -> u64 {
        self.project_info.global_end
    }

    fn get_lane_numbers(&self) -> usize {
        self.lane_data.len()
    }

    fn get_lane_len(&self, idx: usize) -> Result<usize, TimeLineViewDataError> {
        if idx >= self.lane_data.len() {
            return Err(TimeLineViewDataError::DataError);
        }
        Ok(self.lane_data[idx].len())
    }

    fn get_lane_item(
        &self,
        lane_idx: usize,
        item_idx: usize,
    ) -> Result<&'a LaneItem, TimeLineViewDataError> {
        if lane_idx >= self.lane_data.len() || item_idx >= self.lane_data[lane_idx].len() {
            return Err(TimeLineViewDataError::DataError);
        }
        Ok(&self.lane_data[lane_idx][item_idx])
    }

    fn get_lane_item_name(&self, hash_or_idx: &HashOrIdx) -> Result<&str, TimeLineViewDataError> {
        match hash_or_idx {
            HashOrIdx::Hash(h) => {
                return Ok(ui::strip_json_name(
                    match self.project_info.files_hash.get(h) {
                        Some(str) => str,
                        None => return Err(TimeLineViewDataError::DataError),
                    },
                ))
            }
            HashOrIdx::Idx(i) => {
                if *i as usize >= self.project_info.non_files.len() {
                    return Err(TimeLineViewDataError::DataError);
                }
                Ok(&self.project_info.non_files[*i as usize])
                // + &format!("{:.2?}", Duration::from_micros(hi.dur))
            }
        }
    }
}

impl DataView for ProjectTimeline {
    fn render(&mut self, project_info: &ProjectInfo, ui: &mut egui::Ui) -> DataViewResponse {
        let mut response = DataViewResponse::default();

        let data_provider =
            ProjectTimeLineDataProvider::new(project_info, &self.lane_data, &self.active_tu);

        if let Ok(r) = self.time_line_view.render(&data_provider, ui) {
            match r {
                TimeLineViewResponse::DblClicked(h_or_i) => match h_or_i {
                    HashOrIdx::Hash(h) => response.activated_tu = Some(h),
                    HashOrIdx::Idx(_) => {}
                },
                TimeLineViewResponse::Clicked(h_or_i) => match h_or_i {
                    HashOrIdx::Hash(h) => self.active_tu = Some(h),
                    HashOrIdx::Idx(_) => {}
                },
                TimeLineViewResponse::None => (),
            }
        }

        response
    }

    fn initialise(&mut self, project_info: &ProjectInfo) {
        let mut tu_vec = project_info
            .translation_units
            .values()
            .collect::<Vec<&TranslationUnit>>();
        tu_vec.par_sort_unstable_by(|&a, &b| {
            if a.start == b.start {
                (a.start + a.dur).cmp(&(b.start + b.dur))
            } else {
                a.start.cmp(&b.start)
            }
        });
        let mut lanes = Vec::<Vec<LaneItem>>::new();
        for tu in &tu_vec {
            let mut lane_index: usize = 0;
            if !lanes.is_empty() {
                let mut dist = 10e9 as u64;
                let mut l_idx = 0;
                let mut found = false;
                for lane in &lanes {
                    let lane_last = lane.last().unwrap();
                    let lane_end = lane_last.start + lane_last.dur;
                    if tu.start >= lane_end {
                        let d = tu.start - lane_end;
                        if d < dist {
                            dist = d;
                            l_idx = lane_index;
                            found = true;
                        }
                    }
                    lane_index += 1;
                }
                if found {
                    lane_index = l_idx;
                }
            }
            if lane_index == lanes.len() {
                lanes.push(Vec::<LaneItem>::new());
            }
            lanes[lane_index].push(LaneItem {
                hash_or_idx: HashOrIdx::Hash(tu.hash),
                start: tu.start,
                dur: tu.dur,
            });
        }
        self.lane_data = lanes;
    }
}
