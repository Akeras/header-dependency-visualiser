use crate::data::project::{HeaderSum, ProjectInfo};
use crate::ui::{self, DataView, DataViewResponse};
use eframe::egui;
use eframe::emath::{Align2, NumExt};
use rayon::slice::ParallelSliceMut;
use std::time::Duration;

#[derive(Default)]
pub struct TopHeaders {
    top_headers: Vec<HeaderSum>,
    active_header: Option<u64>,
}

impl DataView for TopHeaders {
    fn render(&mut self, project_info: &ProjectInfo, ui: &mut egui::Ui) -> DataViewResponse {
        ui.vertical(|ui| {
            ui.heading("Top headers");
            if self.active_header.is_some() {
                let empty_string = "".to_string();
                let active_header = project_info
                    .files_hash
                    .get(&self.active_header.unwrap())
                    .unwrap_or(&empty_string);
                let active_header_sum = project_info
                    .sum_header_info
                    .get(&self.active_header.unwrap())
                    .unwrap();

                ui.label(format!(
                    "{}: (avg time: {:.2?}, inclusions: {}, total time: {:.2?})",
                    ui::strip_path(active_header),
                    Duration::from_micros(active_header_sum.dur / active_header_sum.inc),
                    active_header_sum.inc,
                    Duration::from_micros(active_header_sum.dur),
                ));
            } else {
                ui.label(" ");
            }
        });

        let mut response = DataViewResponse::default();
        egui::ScrollArea::vertical()
            .auto_shrink([false; 2])
            .show_viewport(ui, |ui, viewport| {
                ui.set_height(2_000.0);
                let x = ui.min_rect().left();
                let y = ui.min_rect().top();

                let mut hover_pos: Option<egui::Pos2> = None;
                let mut clicked_pos: Option<egui::Pos2> = None;
                let mut dbl_clicked_pos: Option<egui::Pos2> = None;
                ui.input(|i| {
                    if i.pointer.has_pointer() {
                        hover_pos = i.pointer.hover_pos();
                    }
                    if i.pointer.primary_clicked() {
                        clicked_pos = i.pointer.interact_pos();
                    }
                    if i.pointer
                        .button_double_clicked(egui::PointerButton::Primary)
                    {
                        dbl_clicked_pos = i.pointer.interact_pos();
                    }
                });
                let vp_width = viewport.width();
                let d_width = (self.top_headers[0].dur / 1000) as f32;

                let x_padding = 2.0;

                let mut y_idx = 0.0;
                for header in &self.top_headers {
                    let sy = y + y_idx * 20.0;
                    let ey = sy + 18.0;

                    let sx = x + x_padding;
                    let ex = x + vp_width * (header.dur / 1000) as f32 / d_width - x_padding;

                    let tu_rect = egui::Rect::from_min_max(egui::pos2(sx, sy), egui::pos2(ex, ey));
                    let hovered = match hover_pos {
                        Some(pos) => tu_rect.contains(pos),
                        None => false,
                    };
                    let clicked = match clicked_pos {
                        Some(pos) => tu_rect.contains(pos),
                        None => false,
                    };
                    if clicked {
                        self.active_header = Some(header.hash);
                    }
                    let dbl_clicked = match dbl_clicked_pos {
                        Some(pos) => tu_rect.contains(pos),
                        None => false,
                    };
                    if dbl_clicked {
                        response.activated_header = Some(header.hash);
                    }
                    let active = match self.active_header {
                        Some(h) => header.hash == h,
                        None => false,
                    };
                    let extra_color = if hovered {
                        64
                    } else if active {
                        32
                    } else {
                        0
                    };

                    ui.painter().rect(
                        tu_rect,
                        2.0,
                        egui::Color32::from_rgb(
                            ((header.hash % 16) as u8) * 4 + 16 + extra_color,
                            extra_color,
                            extra_color,
                        ),
                        if active {
                            egui::Stroke::new(1.0, egui::Color32::WHITE)
                        } else {
                            egui::Stroke::new(0.0, egui::Color32::TRANSPARENT)
                        },
                    );

                    let name = ui::strip_path(project_info.files_hash.get(&header.hash).unwrap());

                    let my_font_id = egui::FontId::new(10.0, egui::FontFamily::Monospace);
                    let my_color = egui::Color32::from_rgb(192, 192, 192);
                    ui.painter().text(
                        egui::pos2(sx + 5.0, (sy + ey) / 2.0),
                        Align2::LEFT_CENTER,
                        ui::ellide_text(name, ex - sx - 10.0, ui, &my_font_id),
                        my_font_id,
                        my_color,
                    );
                    y_idx += 1.0;
                }
            });
        response
    }

    fn initialise(&mut self, project_info: &ProjectInfo) {
        let top_headers = &mut self.top_headers;
        top_headers.clear();

        top_headers.reserve(project_info.sum_header_info.len());
        project_info.sum_header_info.values().for_each(|v| {
            top_headers.push((*v).clone());
        });
        top_headers.par_sort_unstable_by(|a, b| b.dur.cmp(&a.dur));
        let new_len: usize = 100.at_most(top_headers.len());
        top_headers.resize_with(new_len, HeaderSum::default);
    }
}
