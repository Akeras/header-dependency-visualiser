use crate::data::project::{ProjectInfo, TranslationUnit};
use crate::ui::{self, DataView, DataViewResponse};
use eframe::egui;
use eframe::emath::{Align2, NumExt};
use rayon::slice::ParallelSliceMut;
use std::time::Duration;

#[derive(Default)]
pub struct TopTranslationUnits {
    top_tus: Vec<TranslationUnit>,
    active_tu: Option<u64>,
}

impl DataView for TopTranslationUnits {
    fn render(&mut self, project_info: &ProjectInfo, ui: &mut egui::Ui) -> DataViewResponse {
        let mut response = DataViewResponse::default();
        ui.vertical(|ui| {
            ui.heading("Top translation units");
            if self.active_tu.is_some() {
                let empty_string = "".to_string();
                let active_tu = project_info
                    .files_hash
                    .get(&self.active_tu.unwrap())
                    .unwrap_or(&empty_string);
                let avg = project_info
                    .translation_units
                    .get(&self.active_tu.unwrap())
                    .unwrap()
                    .dur;

                ui.label(format!(
                    "{}: {:.2?}",
                    ui::strip_json_name(active_tu),
                    Duration::from_micros(avg)
                ));
            } else {
                ui.label(" ");
            }
        });

        egui::ScrollArea::vertical()
            .auto_shrink([false; 2])
            .show_viewport(ui, |ui, viewport| {
                ui.set_height(2_000.0);
                let x = ui.min_rect().left();
                let y = ui.min_rect().top();

                let mut hover_pos: Option<egui::Pos2> = None;
                let mut clicked_pos: Option<egui::Pos2> = None;
                let mut dbl_clicked_pos: Option<egui::Pos2> = None;
                ui.input(|i| {
                    if i.pointer.has_pointer() {
                        hover_pos = i.pointer.hover_pos();
                    }
                    if i.pointer.primary_clicked() {
                        clicked_pos = i.pointer.interact_pos();
                    }
                    if i.pointer
                        .button_double_clicked(egui::PointerButton::Primary)
                    {
                        dbl_clicked_pos = i.pointer.interact_pos();
                    }
                });
                let vp_width = viewport.width();
                let d_width = (self.top_tus[0].dur / 1000) as f32;

                let x_padding = 2.0;

                let mut y_idx = 0.0;
                for tu in &self.top_tus {
                    let sy = y + y_idx * 20.0;
                    let ey = sy + 18.0;

                    let sx = x + x_padding;
                    let ex = x + vp_width * (tu.dur / 1000) as f32 / d_width - x_padding;

                    let tu_rect = egui::Rect::from_min_max(egui::pos2(sx, sy), egui::pos2(ex, ey));
                    let hovered = match hover_pos {
                        Some(pos) => tu_rect.contains(pos),
                        None => false,
                    };
                    let clicked = match clicked_pos {
                        Some(pos) => tu_rect.contains(pos),
                        None => false,
                    };
                    if match dbl_clicked_pos {
                        Some(pos) => tu_rect.contains(pos),
                        None => false,
                    } {
                        response.activated_tu = Some(tu.hash);
                    }
                    if clicked {
                        self.active_tu = Some(tu.hash);
                    }
                    let active = match self.active_tu {
                        Some(t) => tu.hash == t,
                        None => false,
                    };
                    let extra_color = if hovered {
                        64
                    } else if active {
                        32
                    } else {
                        0
                    };

                    ui.painter().rect(
                        tu_rect,
                        2.0,
                        egui::Color32::from_rgb(
                            ((tu.hash % 16) as u8) * 4 + 16 + extra_color,
                            extra_color,
                            extra_color,
                        ),
                        if active {
                            egui::Stroke::new(1.0, egui::Color32::WHITE)
                        } else {
                            egui::Stroke::new(0.0, egui::Color32::TRANSPARENT)
                        },
                    );

                    let name = ui::strip_json_name(project_info.files_hash.get(&tu.hash).unwrap());

                    let my_font_id = egui::FontId::new(10.0, egui::FontFamily::Monospace);
                    let my_color = egui::Color32::from_rgb(192, 192, 192);
                    ui.painter().text(
                        egui::pos2(sx + 5.0, (sy + ey) / 2.0),
                        Align2::LEFT_CENTER,
                        ui::ellide_text(name, ex - sx - 10.0, ui, &my_font_id),
                        my_font_id,
                        my_color,
                    );
                    y_idx += 1.0;
                }
            });
        response
    }

    fn initialise(&mut self, project_info: &ProjectInfo) {
        let top_tus = &mut self.top_tus;
        top_tus.clear();

        top_tus.reserve(project_info.translation_units.len());
        project_info.translation_units.values().for_each(|v| {
            top_tus.push((*v).clone());
        });
        top_tus.par_sort_unstable_by(|a, b| b.dur.cmp(&a.dur));
        let new_len: usize = 100.at_most(top_tus.len());
        top_tus.resize_with(new_len, TranslationUnit::default);
    }
}
