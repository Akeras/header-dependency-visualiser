#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")] // hide console window on Windows in release

mod data;
mod ui;

use crate::ui::hdv_app::HDVApp;
use eframe::egui;

fn main() {
    let options = eframe::NativeOptions {
        initial_window_size: Some(egui::vec2(1280.0, 960.0)),
        ..Default::default()
    };
    let _res = eframe::run_native(
        "Header Dependency Visualizer",
        options,
        Box::new(|_cc| Box::new(HDVApp::new(_cc))),
    );
}
