use std::collections::{hash_map::DefaultHasher, HashMap};
use std::hash::{Hash, Hasher};

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum HashOrIdx {
    Hash(u64),
    Idx(u64),
}

impl HashOrIdx {
    pub fn value(&self) -> u64 {
        match self {
            HashOrIdx::Hash(h) => *h,
            HashOrIdx::Idx(i) => *i,
        }
    }
}

#[derive(Clone, Debug)]
pub struct HeaderInfo {
    pub translation_unit: u64,
    pub parent_header: u64,
    pub hash_or_idx: HashOrIdx,
    pub start: u64,
    pub dur: u64,
}

impl HeaderInfo {
    fn new(
        translation_unit: u64,
        parent_header: u64,
        hash_or_idx: HashOrIdx,
        start: u64,
        dur: u64,
    ) -> Self {
        HeaderInfo {
            translation_unit,
            parent_header,
            hash_or_idx,
            start,
            dur,
        }
    }
}

#[derive(Default, Clone)]
pub struct HeaderSum {
    pub hash: u64,
    pub dur: u64,
    pub inc: u64,
}

impl HeaderSum {
    pub fn new(hash: u64, dur: u64, inc: u64) -> Self {
        HeaderSum { hash, dur, inc }
    }

    pub fn add(&mut self, dur: u64, inc: u64) {
        self.dur += dur;
        self.inc += inc;
    }
}

#[derive(Default, Clone)]
pub struct TranslationUnit {
    pub hash: u64,
    pub start: u64,
    pub dur: u64,
}

impl TranslationUnit {
    pub fn new(hash: u64, start: u64, dur: u64) -> Self {
        TranslationUnit { hash, start, dur }
    }
}

#[derive(Default)]
pub struct ProjectInfo {
    pub files_hash: HashMap<u64, String>,
    pub non_files: Vec<String>,
    pub header_infos: Vec<HeaderInfo>,
    pub sum_header_info: HashMap<u64, HeaderSum>,
    pub translation_units: HashMap<u64, TranslationUnit>,
    pub totals: HashMap<String, u64>,
    pub global_start: u64,
    pub global_end: u64,
}

impl ProjectInfo {
    pub fn merge(&mut self, o: &mut ProjectInfo) {
        let non_files_len = self.non_files.len();

        self.files_hash.extend(std::mem::take(&mut o.files_hash));
        self.non_files.extend(std::mem::take(&mut o.non_files));
        self.translation_units
            .extend(std::mem::take(&mut o.translation_units));

        self.header_infos.reserve(o.header_infos.len());
        std::mem::take(&mut o.header_infos)
            .into_iter()
            .for_each(|mut hi| {
                hi.hash_or_idx = match hi.hash_or_idx {
                    HashOrIdx::Hash(h) => HashOrIdx::Hash(h),
                    HashOrIdx::Idx(i) => HashOrIdx::Idx(i + non_files_len as u64),
                };
                self.header_infos.push(hi);
            });

        std::mem::take(&mut o.sum_header_info)
            .into_iter()
            .for_each(|(k, v)| {
                self.sum_header_info
                    .entry(k)
                    .and_modify(|hs| {
                        hs.add(v.dur, v.inc);
                    })
                    .or_insert(v);
            });
        if o.global_start != 0 && (self.global_start == 0 || o.global_start < self.global_start) {
            self.global_start = o.global_start;
        }

        if o.global_end > self.global_end {
            self.global_end = o.global_end;
        }

        std::mem::take(&mut o.totals)
            .into_iter()
            .for_each(|(k, v1)| {
                self.totals
                    .entry(k)
                    .and_modify(|v2| *v2 += v1)
                    .or_insert(v1);
            });
    }

    pub fn add_file(&mut self, file_name: &str) -> u64 {
        let hash = calculate_hash(&file_name);
        self.files_hash.entry(hash).or_insert(file_name.to_owned());
        hash
    }

    pub fn add_header_info(&mut self, tu_hash: u64, hash: u64, time: u64, dur: u64) {
        self.header_infos.push(HeaderInfo::new(
            tu_hash,
            0,
            HashOrIdx::Hash(hash),
            time,
            dur,
        ));
        self.sum_header_info
            .entry(hash)
            .and_modify(|shi| {
                shi.add(dur, 1);
            })
            .or_insert(HeaderSum::new(hash, dur, 1));
    }

    pub fn add_translation_unit(&mut self, hash: u64, start: u64, dur: u64) {
        self.translation_units
            .insert(hash, TranslationUnit::new(hash, start, dur));
        if self.global_start == 0 || self.global_start > start {
            self.global_start = start;
        }
        if self.global_end < start + dur {
            self.global_end = start + dur;
        }
    }

    pub fn add_non_header(&mut self, tu_hash: u64, name: String, start: u64, dur: u64) {
        let idx = HashOrIdx::Idx(self.non_files.len() as u64);
        self.non_files.push(name);
        self.header_infos
            .push(HeaderInfo::new(tu_hash, 0, idx, start, dur));
    }

    pub fn add_total(&mut self, name: &str, dur: u64) {
        let parts = name.split(' ').collect::<Vec<&str>>();
        if parts.len() != 2 || parts[0] != "Total" {
            return;
        }
        self.totals
            .entry(parts[1].to_string())
            .and_modify(|v| *v += dur)
            .or_insert(dur);
    }

    pub fn reserve(&mut self, additional: usize) {
        self.header_infos.reserve(additional);
        self.files_hash.reserve(additional);
        self.sum_header_info.reserve(additional);
    }
}

fn calculate_hash<T: Hash + ?Sized>(t: &T) -> u64 {
    let mut s = DefaultHasher::new();
    t.hash(&mut s);
    s.finish()
}
