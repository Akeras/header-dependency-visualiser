use crate::data::project::ProjectInfo;
use serde::Deserialize;
use std::fs;
use std::path::{Path, PathBuf, MAIN_SEPARATOR};
use std::sync::mpsc::Sender;

#[derive(Debug)]
pub enum HdvIoError {
    IoError(std::io::Error),
    SerdeJsonError(serde_json::Error),
}

impl From<std::io::Error> for HdvIoError {
    fn from(err: std::io::Error) -> Self {
        HdvIoError::IoError(err)
    }
}

impl From<serde_json::Error> for HdvIoError {
    fn from(err: serde_json::Error) -> Self {
        HdvIoError::SerdeJsonError(err)
    }
}

#[derive(Deserialize, Default)]
struct TraceEventArgs {
    #[serde(default)]
    detail: String,
}

#[derive(Deserialize)]
struct TraceEvent {
    // pid: u64,
    // tid: u64,
    ph: String,
    #[serde(default)]
    dur: u64,
    ts: u64,
    name: String,
    #[serde(default)]
    args: TraceEventArgs,
}

#[derive(Deserialize)]
struct TraceFile {
    #[serde(rename(deserialize = "traceEvents"))]
    trace_events: Vec<TraceEvent>,
    #[serde(rename(deserialize = "beginningOfTime"))]
    beginning_of_time: u64,
}

#[derive(Deserialize)]
struct CompileCommand {
    command: String,
    //directory: String,
    //file: String,
}

fn process_file(path: &PathBuf, project_info: &mut ProjectInfo) -> Result<(), HdvIoError> {
    let data = fs::read_to_string(path)?;
    let trace_file: TraceFile = serde_json::from_str(&data)?;

    let ev_num = trace_file.trace_events.len();
    project_info.reserve(ev_num);

    let tu_file_name = path.to_string_lossy().to_string();
    let tu_hash = project_info.add_file(&tu_file_name);

    trace_file
        .trace_events
        .iter()
        .filter(|ev| ev.ph == "X")
        .for_each(|ev| {
            if ev.name == "Source" {
                let filename: &str = &ev.args.detail;
                let hash = project_info.add_file(filename);
                project_info.add_header_info(tu_hash, hash, ev.ts, ev.dur);
            } else if ev.name.starts_with("Total") {
                project_info.add_total(&ev.name, ev.dur);
            } else {
                if ev.name == "ExecuteCompiler" {
                    project_info.add_translation_unit(
                        tu_hash,
                        trace_file.beginning_of_time,
                        ev.dur,
                    );
                }
                let name = ev.name.clone() + ": " + &ev.args.detail;
                project_info.add_non_header(tu_hash, name, ev.ts, ev.dur);
            }
        });

    Ok(())
}

pub fn process_files(paths: &[PathBuf]) -> Result<ProjectInfo, HdvIoError> {
    let mut project_info = ProjectInfo::default();
    let _res = paths.iter().try_for_each(|f| -> Result<(), HdvIoError> {
        process_file(f, &mut project_info)?;
        Ok(())
    });

    Ok(project_info)
}

pub fn process_compilation_database(
    dir: &str,
    tx: &Sender<String>,
) -> Result<Vec<PathBuf>, HdvIoError> {
    let mut files = Vec::<PathBuf>::new();

    let separator = MAIN_SEPARATOR.to_string();
    let cc_path_string = dir.to_string() + &separator + "compile_commands.json";
    let cc_path = Path::new(&cc_path_string);

    let data = fs::read_to_string(cc_path)?;
    let compile_commands: Vec<CompileCommand> = serde_json::from_str(&data)?;

    let mut missing_json = 0;
    compile_commands.iter().for_each(|cc| {
        cc.command.split(' ').for_each(|arg| {
            if arg.ends_with(".o") {
                let obj_file = dir.to_string() + &separator + arg;
                let json_file = obj_file.replace(".o", ".json");
                if Path::exists(Path::new(&json_file)) {
                    files.push(PathBuf::from(json_file));
                } else {
                    missing_json += 1;
                }
            }
        })
    });

    if missing_json > 0 {
        tx.send(format!("Missing json trace files: {missing_json}"))
            .unwrap();
    }
    Ok(files)
}
