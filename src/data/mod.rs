mod hdvio;
pub mod project;

use chrono::{DateTime, NaiveDateTime, Utc};
use project::ProjectInfo;
use rayon::prelude::*;
use std::path::PathBuf;
use std::sync::mpsc::Sender;
use std::sync::Mutex;
use std::time::Instant;

use self::hdvio::HdvIoError;

fn timestamp_to_string(timestamp: u64) -> String {
    let naive = NaiveDateTime::from_timestamp_millis(timestamp as i64 / 1000).unwrap();
    let date_time: DateTime<Utc> = DateTime::from_utc(naive, Utc);
    date_time.format("%Y-%m-%d %H:%M:%S").to_string()
}

fn par_process_files(files: &[PathBuf], tx: &Sender<String>) -> Result<ProjectInfo, HdvIoError> {
    let now = Instant::now();
    let mut project_info = ProjectInfo::default();
    let p_i = Mutex::new(Vec::<ProjectInfo>::new());

    // process files in chunks of 512 and move
    // result into vec
    let _res = files
        .par_chunks(512)
        .try_for_each(|f| -> Result<(), HdvIoError> {
            let res = hdvio::process_files(f)?;
            p_i.lock().unwrap().push(res);
            Ok(())
        });

    // merge all results into a single project info
    for i in p_i.lock().unwrap().iter_mut() {
        project_info.merge(i);
    }
    let elapsed = now.elapsed();
    tx.send(format!("Parallel file processing time: {:.2?}", elapsed))
        .unwrap();

    Ok(project_info)
}

fn process_project_with_error(
    build_dir: &str,
    tx: Sender<String>,
) -> Result<ProjectInfo, HdvIoError> {
    let now = Instant::now();
    let start_time = Instant::now();

    let files = hdvio::process_compilation_database(build_dir, &tx)?;

    tx.send(format!("Files found: {}", files.len())).unwrap();
    let elapsed = now.elapsed();
    tx.send(format!(
        "Parsing compilation database time: {:.2?}",
        elapsed
    ))
    .unwrap();

    let project_info = par_process_files(&files, &tx)?;

    tx.send(format!(
        "Header entries: {}",
        project_info.sum_header_info.len()
    ))
    .unwrap();

    let elapsed = start_time.elapsed();
    tx.send(format!("Total processing time: {:.2?}", elapsed))
        .unwrap();

    tx.send(format!(
        "Project start: {} \nProject end: {}",
        timestamp_to_string(project_info.global_start),
        timestamp_to_string(project_info.global_end)
    ))
    .unwrap();

    Ok(project_info)
}

pub fn process_project(build_dir: &str, tx: Sender<String>) -> Option<ProjectInfo> {
    let tx1 = tx.clone();
    match process_project_with_error(build_dir, tx) {
        Ok(res) => Some(res),
        Err(err) => {
            tx1.send(format!("{:?}", err)).unwrap();
            None
        }
    }
}
