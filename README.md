# Header Dependency Visualizer
This little tool helps to visualize and analyze header dependencies and compile times of large scale C++ projects.

# Usage
- Build your C++ project with clang, using the compiler option `-ftime-trace`.
- Make sure that the build system exports the `compile_commands.json` into the build directory..
- Build and launch the app, and set the build directory (the top lever build dir, where `compile_commands.json` was exported to).
- Click `Configure`. After a few seconds the data will be available to view.

# Views
The app is very simple, there are four top level views, and two additional views that can be accessed from the top level views.

## Top level views
### Proejct Overview
This view gives a generic overview about the number of translation units, single threaded total, frontend and backend time. [^1][^2]

![Project overview](assets/HDV_project_overview.png "Project overview")

### Project Timeline
This view shows the compilation time line of each translation unit. Clicking on an item will show the items timing information, double clicking leads to the Translation unit view.[^2]

![Project timeline](assets/HDV_project_timeline.png "Project timeline")

### Top TUs
Here you can see the translation units that contributes the most to the compilation time. Clicking on a translation unit will show the items timing information, double clicking leads to the Translation unit view.[^2]

![Top translation units](assets/HDV_top_tus.png "Top translation units")

### Top headers
This view list the top 100 headers that contribute the most to the compilation time. Clicking a header will show the header contricution to the build time. Double clicking on it will open the Heaviest inclusion of header view.[^2]

![Top headers](assets/HDV_top_headers.png "Top headers")

## Indirect views
### Translation unit view
In this view you can see what happens during compilation of the given translation unit. For header dependencies, the focus should be on Frontend time. [^1] Double clicking on headers will lead to the heaviest inclusion of header view.[^2]

![Translation unit view](assets/HDV_translation_unit.png "Translation unit")

### Heaviest inclusion of header
This view shows in which translation unit was the given header the most expensive. Double clicking on a translation unit will lead to the translation unit view.[^2]

![Heaviest header inclusions](assets/HDV_heaviest_header_inclusions.png "Heaviest header inclusions")

# Notes
[^1]: https://scalablecpp.wordpress.com/2022/03/06/header-dependencies-i/
[^2]: The example screenshots are using the data from building the clang compiler.

